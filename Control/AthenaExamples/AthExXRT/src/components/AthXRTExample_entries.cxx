//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "../VectorAddOCLExampleAlg.h"
#include "../VectorAddXRTExampleAlg.h"
#include "../VectorMultOCLExampleAlg.h"
#include "../VectorMultXRTExampleAlg.h"

// Declare the "components".
DECLARE_COMPONENT(AthExXRT::VectorAddOCLExampleAlg)
DECLARE_COMPONENT(AthExXRT::VectorAddXRTExampleAlg)
DECLARE_COMPONENT(AthExXRT::VectorMultOCLExampleAlg)
DECLARE_COMPONENT(AthExXRT::VectorMultXRTExampleAlg)
